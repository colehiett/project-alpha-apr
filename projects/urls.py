from django.urls import path
from . import views

app_name = "projects"

urlpatterns = [
    path("", views.project_list, name="list_projects"),
    path("<int:id>/", views.show_project, name="show_project"),
    path("create/", views.create_project, name="create_project"),
]
